+++
title = "Accesibility Statement for BK"
description = "This accesibility statement was generated and then I tweaked it a little bit."
template = "different-pages/about.html"

sort_by = "date"
insert_anchor_links = "left"
+++

This is an accessibility statement from BayKanguru.

## Conformance status
The [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/) defines requirements for designers and developers to improve accessibility for people with disabilities. It defines three levels of conformance: Level A, Level AA, and Level AAA.
BK is fully conformant with WCAG 2.1 level AA.
Fully conformant means that the content fully conforms to the accessibility standard without any exceptions.

#### Additional accessibility considerations
Partial or full implementation of the following Level AAA Success Criteria:
- 1.3.6 Identify Purpose
- 1.4.6 Contrast (Enhanced)
- 1.4.8 Visual Presentation
- 2.1.3 Keyboard (No Exception)
- 2.3.2 Three Flashes
- 2.3.3 Animation from Interactions - Added in 2.1
- 2.5.6 Concurrent Input Mechanisms - Added in 2.1

### Feedback
We welcome your feedback on the accessibility of BK.
Please let us know if you encounter accessibility barriers on BK:
- [Codeberg Issues](https://codeberg.org/BayKanguru/pages-zola/issues)

### Technical specifications
Accessibility of BK relies on the following technologies to work with 
the particular combination of web browser and any assistive technologies 
or plugins installed on your computer:
- HTML
- WAI-ARIA
- CSS

These technologies are relied upon for conformance with the accessibility standards used.

### Assessment approach
BayKanguru assessed the accessibility of BK by the following approaches:
- Self-evaluation
- User Reports

### Date
This statement was created on 13 April 2023 using the [W3C Accessibility Statement Generator Tool](https://www.w3.org/WAI/planning/statements/).
