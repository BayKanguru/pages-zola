+++
title = "Looking For a New Typeface"
slug = "looking-for-new-typeface"
description = "I want a new typface. And I need help on finding one. Criteria are in the post."
date = "2021-12-27"

[taxonomies]
tags = ["meta", "typography"]
+++

Current font I'm using, IBM Plex, bored me.
Don't get me wrong, Plex is great looking, it's just, I used it too much so changing it feels like a good idea.
When I find one I'll switch to serving fonts myself too.
'cause only cookie in the site comes from Google Fonts, plus it's google.

## Criteria for the Typeface(s)

It would be cool to get just one typeface but I can accept something else for titles and such.
Whatever they may be typeface should:

1. Be legible
2. Be open source
3. Fit my personal taste

### Legibility

Although it would be so cool to use a [script](https://upload.wikimedia.org/wikipedia/commons/c/c7/Zapfino.svg) or [blackletter](https://upload.wikimedia.org/wikipedia/commons/9/96/Old_English_typeface.svg) font, unfortunately, people will try to read this site.

That aside, new typeface should be legible in small sizes as much as possible. Plus “1, I, l, i”, “B, 8”, “G, Q” and “O, 0” should be distinguishable (what a word).

### Why Open Source?

Mainly because I can't spend any money on a typeface right now.
They are easier to use too, with less restrictive licensing.
And, really, why not?

### Personal Taste

I like serif typefaces better for reading but sans-serif is always welcome.
I don't like just straight lines or uniform widths, a bit of fun is quite appreciated, whatever you get from that.

## Contenders

Here are the fonts that fit those criteria:

- [Latin Modern](https://www.fontsquirrel.com/fonts/Latin-Modern-Roman), a vectorised unicode compatible version of [Donald Knuth](https://cs.stanford.edu/~knuth/)'s Computer Modern.
  And yes, TeX is great.
- [Ubuntu](https://design.ubuntu.com/font/) the font, I just like the look of it and is more legible than a lot of typefaces out there.
- [IBM Plex](https://github.com/IBM/plex/releases/tag/v6.0.0) …

## “I know the perfect typeface!”

How great 🎉!
You can send your suggestions from any of these places [here](@/about/_index.md#contact).
