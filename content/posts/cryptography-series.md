+++
title="Cryptography Series"
date=2022-07-29
description="Starting a series of posts about cryptographic primitives and concepts."

[taxonomies]
tags=["cryptography"]
+++

I decided to write something just to get this site going.
What better way to start than something you like, that thing is cryptography for me.

The idea fascinates me deeply, the fact that you can send a message through a medium that anyone can see, without anyone else seeing the message except the intended reciever.
And that's not it either, some of the cryptographic algorithms have other applications too, such as checksums.

Cryptography is often overlooked by people that—try to—design systems, because properly using (let alone _implementing_) them requires a bit more thought than conventional programs.
This isn't a problem about developers, the subject is inherently hard to fully cover.
Systems generally have a huge attack surface, and covering it fully is hard.

## What is cryptography?

Cryptography is the study of secure communication, even if there are eavesdroppers.
You use cryptographic protocols every day (probably) for example the lock icon next to the URL 
in the modern browsers indicate you are use TLS (**T**ransport **L**ayer **S**ecurity), which is a crytographic protocol for secure communication in computer networks.

## What will I explain?
I will explain how a secure communications system is made.

Without knowing what we want to achieve, we can't learn about these protocols.
Let me list some goals for a secure communication.
- *Encrypt* a message, to lock it, basically, so that only people that have a *key* can *decrypt* or unlock it.
- Share that *key* securely—without anyone else knowing—even if everyone can see anything that communicators send to each other.
- Verify the integrity of the message, to be sure that you are reading the correct message.
- Verify that the person you are communicating to is *actually* the person you *think* you are communicating with.

And I'll explain how you can *store* some data securely too.

## How will I explain?

I'll try not to get too deep into math, even when I do I won't give an in depth version, rather an _analogue_ that has similar properties.
Each part will probably cover a single primitive and expand on previous parts.
