+++
title="Print Styles"
date=2023-04-20
description="Explaining print styles with usable code."

[taxonomies]
tags=["web", "css", "js", "print"]
+++

If you try to print this page you will see that many things change.
This post explains how you do that.

## Why?
Imagine a recipe website, filled with ads, unnecessary log-ins and huge banners everywhere.
There is a good chance that you'd want to print that site but you don't want to have big wasted space all over your papers.
That's where print styles come in.
They make the printed page look like it was written to be printed on a paper.

Now you might why that's relevant for this site in particular.
It isn't, but that is not the point here.
Point is, uh, to learn things along the way 😅.

## Considerations
Now that we questioned our reasons to have this feature lets see what I considered while building it.

1. A print style should remove all things irrelevant to the content.
2. Printed page should be _as useful_ as the website.

You may interpret the first point in a variety of ways, do what fits your site.
I remove copy buttons on code blocks, the header and the search bar.
I also reset almost all text styling to default.
Only thing left in the site that might be considered irrelevant is the footer.

For the second point, one important thing is that anchor links do not actually show up in print.
As an example [this link right here](@/_index.md) would just show up as “this link right here” with no indication on where it's pointing.
The simplest way to fix this is to adapt this snippet to your site:
```css
@media print {
  a[href]::after {
    content: " <" attr(href) ">";
  }
}
```
I still have this on my site as a fallback when javascript is not available. 
It is not what you're seeing when you print this page, because [print.js](/print.js) on my site adds links as a list at the end of content inspired by [Adrian Roselli](https://adrianroselli.com/2018/09/links-list-for-print-styles.html) (Use [NoScript](https://noscript.net/) to try).
I think the javascript version looks better however fallbacks are necessary.

If you'd like to have this style in a seperate file, you can use something like:
```html
<link rel="stylesheet" href="style.css" media="screen"/>
<link rel="stylesheet" href="print.css" media="print"/>
```
This loads and unloads styles as `media` changes and keeps both styles completely seperate.

## Code
I couldn't leave you only with an explanation.

Before taking code from here, consider writing these yourself, like I said “to learn things along the way”.
First of all, make what you can with CSS that way you have way less to mess up.
When you're sure that what you want to do can't be done in an accessible and performant way in CSS, only then should you start writing javascript.

There are 2 [events](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events) that you have to know when modifying print content with javascript: `beforeprint` and `afterprint`.
As the names suggest these fire before and after the print, former to modify the site and latter to clean up afterwards.

### Links in Footer
You can copy and paste this, then change the `querySelector`s to match your site assuming:
- Every selected anchor can have a `<sup>` appended to it without issues.
- Selected content can have a `<footer>` appended to it without issues.

```js
let links = {
  content: document.querySelector(/* your content selector */),
  anchors: document.querySelectorAll(/* your anchor selector */),
  footer: undefined,

  init_footer: () => {
    links.footer = document.createElement("footer");
    links.footer.setAttribute("class", "links");
    let list = document.createElement("ol");
    list.setAttribute("class", "print-links");
    // See: https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html
    // :)
    list.setAttribute("start", 0);
    links.footer.append(list);
  },

  beforeprint: () => {
    links.init_footer();
    links.content.append(links.footer);

    for (let i = 0; i < links.anchors.length; i += 1) {
      // add a little number next to the link
      const link_number = document.createElement("sup");
      link_number.append(i)
      links.anchors[i].append(link_number);

      // add the link to the list
      const href = links.anchors[i].getAttribute("href");
      const li = document.createElement("li");
      li.innerText = href;
      links.footer.firstChild.append(li);
    }

    // mark the content so CSS can detect our list easily
    links.content.setAttribute("print-links", "");
  },
  afterprint: () => {
    links.footer.remove();
    links.footer = undefined;

    for (const anchor of links.anchors) {
      anchor.removeChild(anchor.lastChild);
    }

    links.content.removeAttribute("print-links");
  },
}

window.addEventListener("beforeprint", links.beforeprint);
window.addEventListener("afterprint", links.afterprint);
```

Then I would change the previous CSS snippet to something like this:
```css
@media print {
  /*your content selector */:not([print-links]) a[href]::after {
    content: " <" attr(href) ">";
  }
}
```
Which would disable appended links if javascript works.

### Open Table of Contents `<details>`
This is easily copy-pastable if you change the `querySelector` to match your table's.
```js
let toc = document.querySelector(/* your toc selector */);
toc.was_open = false;
toc.beforeprint = () => {
  toc.was_open = toc.hasAttribute("open");
  toc.setAttribute("open", "");
};
toc.afterprint = () => {
  if (!toc.was_open) { toc.removeAttribute("open"); }
};

window.addEventListener("beforeprint", toc.beforeprint);
window.addEventListener("afterprint", toc.afterprint);
```
This restores the last state, meaning if `toc` was already open, it will stay open after print.
