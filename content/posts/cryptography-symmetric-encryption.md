+++
title="Symmetric Encryption"
date=2022-07-30
description="Covering symmetric encryption, a fundamental part of cryptography."

[taxonomies]
tags=["cryptography"]
+++

Symmetric encryption is one of the base parts of cryptography.
Lot of well known things such as [Engima machine](https://en.wikipedia.org/wiki/Enigma_machine)
and [Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher),
as well as lesser known algorithms like [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)
and [ChaCha](https://cr.yp.to/chacha.html) fall into this category.
These are all do basically the same thing, they use a key to turn a message like “Hello” into nonsense like “Nkrru” that only someone with the correct key can make sense of.
(FYI: this was caesar cipher on English alphabet with 6 to end as key.)

It's _symmetric_ because you use the same key for encryption and decryption as opposed to _asymmetric_ encryption which uses different keys.

## Basic Requirements

- Algorithm needs to be reversible, for _decryption_ or unlocking.
- Encrypted form-which I will call “ciphertext” from now on—needs to be as nonsensical as possible.
  It would be best If it was indistinguishable from random noise.
- The _key_ needs to be hard to guess.

An algorithm that has all these properties could be used to encrypt messages, but that doesn't necessarily mean that any algorithm with these properties is _secure_.
A great advise is **never invent your own cryptographic algoritms** unless, of course, for educational purposes or if you are an expert in the field.

## How Does It Work?

I'll provide some examples for cryptographic algorithms in this section.

All code will be in [Python](https://www.python.org/) unless mentioned otherwise.
Using python should mean even people without prior programming experience should understand the point.

## Caesar Cipher

Caesar cipher is as simple as it gets, your key defines how much you shift a letter alphabetically.
As in the example in the introduction, 6 to end means, from H you count to the end of the alphabet 6 letters—I J K L M _N_ —that's it.
If you reach the end, you roll back the other side.
Decryption is just as simple, you count to the opposite side.

That means your key size is effectively size of your alphabet.
The best you can do is add more letters to the alphabet, like adding numbers, symbols or differentiating between capital and lowercase letters.

### Python Implementation

Doing this part yourself might let the idea better sink in.
If you ever get stuck, just check the [full code](/codes/cryptography/caesar_cipher.py) out.

```python
# I'll define a large alphabet to make it as good as possible
ALPHABET = \
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!? "
```

We should go through each letter and shift it.

```python
ciphertext = ""
for char in plaintext:
    index = ALPHABET.find(char)  # find number of the letter

    # add key to index and wrap
    new_index = (index + key) % len(ALPHABET)
    ciphertext += ALPHABET[new_index]  # append to result
```

That `%` is remainder from division, so we wrap at the ends of alphabet.

Decryption is just as simple.

```python
plaintext = ""
for char in ciphertext:
    index = ALPHABET.find(char)  # find number of the letter

    # subtract key from index and wrap
    old_index = (index - key) % len(ALPHABET)
    plaintext += ALPHABET[old_index]  # append to result
```

Again same thing in reverse.
You can find the full source code [here](/codes/cryptography/caesar_cipher.py).

### Drawbacks

Because of its simplicity, caesar cipher comes with many vulnerabilities.

First of all you could easily try all possible keys and see which output made sense.
To fix it, you would need a larger key size.

Second, caesar leaks some information about the underlying message.
You can see from `Nkrru` that the third and fourth letters are the same.
Solving that requires more thought but isn't impossible.

In the next part you will see an example made by me.
It was supposed to be before caesar cipher, but more I looked at it, it felt like it's more advanced.

## Handcrafted Example

Algorithm presented here is neither going to be nor
intended to be secure in any way but you will learn a lot hopefully.

Let's start with the algorithm.
You've learned in the primary school that subtraction is reverse addition, division is reverse multiplication and so forth.
As an example:

```python
x = 3.14
y = x + 5 - 5 # y should be equal to x
```

And these properties can be combined:

```python
x = 3.14
y = (5*x - 6) / 8 # do some calculations
z = (8*y + 6) / 5 # reverse order to get our initial value back
```

Another operation that is perfect for these is XOR (Exclusive or).
It's amazing because if you apply XOR twice with the same number, you get the initial value back.
So no reverse operation or order required.

```python
x = 37
# In python XOR is denoted ^
y = x ^ 5 ^ 5 # this is just x again
```

### XOR

Let's digress for a little bit to explain what XOR is.

XOR is a logical operation and its truth table is as such:

<table aria-describedby="table1-description">
    <thead>
        <tr>
            <th>p</th>
            <th>q</th>
            <th>XOR</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <td>0</td>
            <td>1</td>
            <td>1</td>
        </tr>
        <tr>
            <td>1</td>
            <td>0</td>
            <td>1</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1</td>
            <td>0</td>
        </tr>
    </tbody>
</table>

<p id="table1-description">
    For those that don't know about truth tables and for accessibility purposes here is a description of that.
    There is a branch of math called logic, in it you use “true” and “false” values which are repsesented 1 and 0 correspondingly.
    In logic we have 2 basic operations called AND and OR.
    AND gives 1 if both operands are 1 and else 0 like this `1 AND 1 = 1` whereas `0 AND 1 = 1`.
    OR gives 0 if both operands are 0 and else 1 like this `0 OR 0 = 0` whereas `0 OR 1 = 1`.
    XOR is OR that gives 0 in the case that both operands are 1. 
    In other words XOR is only true if operands are different.
</p>

Now we know what XOR is, but how can it be applied to numbers, if it is a logical operation.
I'm glad you've asked, see, numbers are stored as 1's and 0's which we call “bit”s, when applying XOR to numbers it operates on number's individual bits.
An example would be:

```
1100 -> 12
1010 -> 10
---- XOR     ------------|
0110 -> 6 is the result  | Here we use the same numbers
1010 -> 10   ------------|
---- XOR
1100 -> 12 is the result, which was our initial number
```

Hopefully with this you've understood what XOR is.

### Real Code

Now that we now what we can use, let's combine them to make an encryption algorithm for educational purposes.
You can find the full code [here](/codes/cryptography/symmetric_cipher.py).

Encryption part is like this.

```python
ciphertext = []
for byte in plaintext:
    encrypted = byte ^ key  # xor the byte with the key
    tmp.append(encrypted)

    # modify the key for the next round
    key = encrypted * (key+1) % 128
```

You know about the XOR part now, but what is with the last line?
I added that part to modify key before each round.
Doing that each character in our ciphertext now depends on every previous character too,
eliminating a information leak like the one from caesar cipher.

Decryption is just encryption done in reverse.

```python
plaintext = []
for byte in ciphertext:
    tmp.append(byte ^ key)

    # re-do the modification on key
    # this time byte is the encrypted data
    key = byte * (key+1) % 128
```

Again, last part is doing the key modification.

## Getting Serious

With those 2 examples, you probably have some grasp on what encryption algorithms are doing.
Now, I'm going to tell you about what people actually—should—use.
I'm not going to get in detail, but I'll provide some links if you want to learn more.

### AES

AES stands for **A**dvanced **E**ncryption **S**tandard, very creative huh…
It was selected in 2000 in the Advanced Encryption Standard
process by NIST (National Institute of Standards and Technology).
Before it was selected it was called “Rijndael”.

It has seen a lot of attention from cryptanalysts and it stood the test of time.
Nowadays x86 CPUs (Central Processing Unit)—that means basically all desktop CPUs,
currently—have a dedicated co-processors to run the algorithm very fast.

### ChaCha

ChaCha released after the NIST process was finished,
its software implementations are faster than AES that
means it's perfect for processors without dedicated AES co-processors like smartphones.

### What should I use then?

Wait!
There is more to cryptography than this.
If you use these on their own, you can't verify the integrity of your message.
People can change your exteremely important message
from “I LOVE you!” to “I HATE you!”, and no one will notice.
Truly horrifying, I know 🙂.

There are solutions and I will cover them going forward.

## Further Material

- AES
  - [The standard.](nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197.pdf)
  - [Computerphile video on the subject.](https://www.youtube.com/watch?v=O4xNJsjtN6E)
  - [Wikipedia article.](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)
- ChaCha
  - [Original paper and some implementations.](https://cr.yp.to/chacha.html)
  - [Computerphile video on the subject.](https://www.youtube.com/watch?v=UeIpq-C-GSA)
  - [Section of the Wikipedia article.](https://en.wikipedia.org/wiki/Salsa20#ChaCha_variant)

I'll hopefully come with another post soon, explaining another part of cryptography in this fashion.
Until then, bye 👋!
