+++
title = "About"
description = "Things about me and this site."
template = "different-pages/about.html"

sort_by = "date"
insert_anchor_links = "left"
+++

## Me
I'm a middleschool student that is trying to learn some things about programming 💻.
I like programming things that at least have some math in it.

Here is what I use on my computer.
- [Arch Linux](https://archlinux.org/) (btw :D)
- [Helix Editor](https://helix-editor.com/)
- [LibreWolf](https://librewolf.net/) as a browser
- [Mastodon](https://joinmastodon.org/)

## This Site
This site is:
- Powered by [Zola](https://www.getzola.org/) which is a static site generator
- Hosted on [Codeberg Pages](https://codeberg.page/)

I chose Zola mainly to try, but it turns out it's pretty good so I keep going with it.
And since I didn't need more than a static site, Codeberg Pages seemed like a great choice.

Style of this site is vaguely inspired by [XXIIVV](https://wiki.xxiivv.com/site/).
That site is quite beautiful to me even with 2 colours.

## Contact
- [Codeberg](https://codeberg.org/BayKanguru)
- <a rel="me" href="https://todon.eu/@baykanguru">Mastodon</a>
- XMPP: baykanguru@xmpp.jp
