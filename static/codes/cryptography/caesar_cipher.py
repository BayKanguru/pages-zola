#!/bin/python
"""
    An educational caesar cipher implementation.
    I could've wrote it more pythonic or efficient.
    I deliberately didn't because of the readability.
"""

ALPHABET = \
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!? "


def encrypt(plaintext: str, key: int) -> str:
    """Encrypt given string."""
    ciphertext = ""
    for char in plaintext:
        index = ALPHABET.find(char)  # find number of the letter

        # add key to index and wrap
        new_index = (index + key) % len(ALPHABET)
        ciphertext += ALPHABET[new_index]  # append to result
    return ciphertext


def decrypt(ciphertext: str, key: int) -> str:
    """Decrypt given string."""
    plaintext = ""
    for char in ciphertext:
        index = ALPHABET.find(char)  # find number of the letter

        # subtract key from index and wrap
        old_index = (index - key) % len(ALPHABET)
        plaintext += ALPHABET[old_index]  # append to result
    return plaintext


if __name__ == "__main__":
    message = "Hello, World!"
    key = 6
    print("Initial message: " + message)

    ciphertext = encrypt(message, key)
    print("Ciphertext: " + ciphertext)

    plaintext = decrypt(ciphertext, key)
    print("Decrypted plaintext: " + plaintext)
