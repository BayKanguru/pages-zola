#!/bin/python3
"""
    An educational symmetric cipher.
    This is not intended to be secure, DO NOT USE IN PRODUCTION.
"""


def encrypt(plaintext: bytes, key: int) -> list[int]:
    """Encrypt a given plaintext."""
    tmp = []
    for byte in plaintext:
        encrypted = byte ^ key  # xor the byte with the key
        tmp.append(encrypted)

        # modify the key for the next round
        key = encrypted * (key+1) % 128
    return tmp


def decrypt(ciphertext: bytes, key: int) -> list[int]:
    """Decrypt a given plaintext."""
    tmp = []
    for byte in ciphertext:
        tmp.append(byte ^ key)

        # re-do the modification on key
        # this time byte is the encrypted data
        key = byte * (key+1) % 128
    return tmp


if __name__ == "__main__":
    MESSAGE = "Hello, World!".encode("ascii")
    print(MESSAGE)
    key = 10

    enc_bytes = encrypt(MESSAGE, key)
    print(enc_bytes)
    encrypted = bytes(enc_bytes)

    decrypted = bytes(decrypt(encrypted, key)).decode("ascii")
    print(decrypted)
