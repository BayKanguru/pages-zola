let links = {
  content: document.querySelector(".content"),
  anchors: document.querySelectorAll(".content a[href]:not(.tag, .anchor)"),
  footer: undefined,

  init_footer: () => {
    links.footer = document.createElement("footer");
    links.footer.setAttribute("class", "links");
    let list = document.createElement("ol");
    list.setAttribute("class", "print-links");
    // See: https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html
    // :)
    list.setAttribute("start", 0);
    links.footer.append(list);
  },

  beforeprint: () => {
    links.init_footer();
    links.content.append(links.footer);

    for (let i = 0; i < links.anchors.length; i += 1) {
      // add a little number next to the link
      const link_number = document.createElement("sup");
      link_number.append(i)
      links.anchors[i].append(link_number);

      // add the link to the list
      const href = links.anchors[i].getAttribute("href");
      const li = document.createElement("li");
      li.innerText = href;
      links.footer.firstChild.append(li);
    }

    // mark the content so CSS can detect our list easily
    links.content.setAttribute("print-links", "");
  },
  afterprint: () => {
    links.footer.remove();
    links.footer = undefined;

    for (const anchor of links.anchors) {
      anchor.removeChild(anchor.lastChild);
    }

    links.content.removeAttribute("print-links");
  },
}

window.addEventListener("beforeprint", links.beforeprint);
window.addEventListener("afterprint", links.afterprint);

let toc = document.querySelector(".toc");
toc.was_open = false;
toc.beforeprint = () => {
  toc.was_open = toc.hasAttribute("open");
  toc.setAttribute("open", "");
};
toc.afterprint = () => {
  if (!toc.was_open) { toc.removeAttribute("open"); }
};

window.addEventListener("beforeprint", toc.beforeprint);
window.addEventListener("afterprint", toc.afterprint);
